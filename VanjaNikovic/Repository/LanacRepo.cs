﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VanjaNikovic.Interfaces;
using VanjaNikovic.Models;

namespace VanjaNikovic.Repository
{
    public class LanacRepo:ILanacRepo,IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Lanac> GetAll()
        {
            return db.Lanci;
        }

        public Lanac GetById(int id)
        {
            return db.Lanci.FirstOrDefault(z=>z.Id==id);

        }

        public IEnumerable<Lanac> GetTradicija()
        {
            return db.Lanci.OrderBy(z => z.Godina).Take(2);
        }

        public IEnumerable<LanciZaposleni> GetZaposleni()
        {
            var zaposleni = db.Hoteli.GroupBy(z => z.LanacId).Select(x => new { key = x.Key, broj = x.Sum(z => z.BrojZaposlenih) });
            var hoteli = from l in db.Lanci
                         join z in zaposleni
                         on l.Id equals z.key
                         select new LanciZaposleni
                         {
                             Id = l.Id,
                             Naziv = l.Naziv,
                             Godina = l.Godina,
                             Broj = z.broj
                         };
            return hoteli.OrderByDescending(z=>z.Broj);


        }

        public IEnumerable<LanciPost> PostUkupanBrojSoba(int granica)
        {
            var brojSoba = db.Hoteli.GroupBy(z => z.LanacId).Select(x => new { key = x.Key, broj = x.Sum(z => z.BrojSoba) });
            var hoteli = from l in db.Lanci
                         join z in brojSoba
                         on l.Id equals z.key
                         select new LanciPost
                         {
                             Id = l.Id,
                             Naziv = l.Naziv,
                             Godina = l.Godina,
                             BrojSoba = z.broj
                         };
            return hoteli.Where(x=>x.BrojSoba>granica).OrderBy(x=>x.BrojSoba);
        }

      
    }
}