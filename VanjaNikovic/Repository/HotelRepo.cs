﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using VanjaNikovic.Interfaces;
using VanjaNikovic.Models;


namespace VanjaNikovic.Repository
{
    public class HotelRepo:IHotelRepo,IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Hotel> GetAll()
        {
            return db.Hoteli.Include("Lanac").OrderBy(x => x.Godina);
        }

        public IEnumerable<Hotel> GetMinimum(int zaposleni)
        {
            return db.Hoteli.Include("Lanac").Where(x=>x.BrojZaposlenih>=zaposleni).OrderBy(x => x.BrojZaposlenih);

        }

        public IEnumerable<Hotel> GetPretraga(Pretraga pretraga)
        {
            var hoteli = db.Hoteli.Include("Lanac").Where(x => x.BrojSoba >= pretraga.Najmanje);
            if(pretraga.Najvise!=null)
            {
                hoteli = hoteli.Where(x => x.BrojSoba <= pretraga.Najvise);
            }
            return hoteli.OrderByDescending(z => z.BrojSoba);
        }

        public Hotel GetById(int id)
        {
            return db.Hoteli.Include("Lanac").FirstOrDefault(x => x.Id == id);
        }

        public void Add(Hotel hotel)
        {
            db.Hoteli.Add(hotel);
            db.SaveChanges();
        }

            public void Update(Hotel hotel)
        {
            db.Entry(hotel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Hotel hotel)
        {
            db.Hoteli.Remove(hotel);
            db.SaveChanges();
        }
    }
}