﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanjaNikovic.Models;

namespace VanjaNikovic.Interfaces
{
    public interface ILanacRepo
    {
        IEnumerable<Lanac> GetAll();
        Lanac GetById(int id);
        IEnumerable<Lanac> GetTradicija();
        IEnumerable<LanciZaposleni> GetZaposleni();
        IEnumerable<LanciPost> PostUkupanBrojSoba(int granica);


    }
}
