﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanjaNikovic.Models;

namespace VanjaNikovic.Interfaces
{
    public interface IHotelRepo
    {
        IEnumerable<Hotel> GetAll();
        IEnumerable<Hotel> GetMinimum( int zaposleni);
        IEnumerable<Hotel> GetPretraga(Pretraga pretraga);

        Hotel GetById(int id);
        void Add(Hotel hotel);
        void Update(Hotel hotel);
        void Delete(Hotel hotel);
    }
}
