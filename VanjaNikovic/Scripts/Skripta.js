﻿$(document).ready(function () {

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var hotEndpoint = "/api/hoteli/";
    

    // okidanje ucitavanja proizvoda
    loadHot();



    // pripremanje dogadjaja za izmenu i brisanje
    $("body").on("click", "#btnDelete", deleteHot);
    

    // ucitavanje proizvoda
    function loadHot() {
      
        var requestUrl = 'https://' + host + hotEndpoint;
        $.getJSON(requestUrl, setHot);
    };

    
    function setHot(data, status) {
        $("#ucitavanjeDiv").css("display", "block");
        $("#lanciSaTradicijom").css("display", "none");

        var $container = $("#dataHoteli");
        $container.empty();

        if (status == "success") {
            // ispis naslova
            var div = $("<div></div>");
            var h1 = $("<h1 class='text-center m-4'>Hoteli</h1>");
            div.append(h1);
            // ispis tabele
            var table = $("<table class='table table-dark table-hover m-3'></table>");
            if (token) {
                var header = $("<thead><tr class=''><td>Naziv</td><td>Godina osnivanja</td><td>Broj soba</td><td>Broj zaposlenih</td><td>Lanac</td><td>Brisanje</td></tr></thead>");
            } else {
                var header = $("<thead><tr class=''><td>Naziv</td><td>Godina osnivanja</td><td>Broj soba</td><td>Broj zaposlenih</td><td>Lanac</td></tr></thead>");
            }

            table.append(header);
            tbody = $("<tbody></tbody>");
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Naziv + "</td><td>" + data[i].Godina + "</td><td>" + data[i].BrojSoba + "</td><td>" + data[i].BrojZaposlenih + "</td><td>" + data[i].Lanac.Naziv +"</td>";
                // prikaz dugmadi za izmenu i brisanje
                var stringId = data[i].Id.toString();
                var displayDelete = "<td><button id=btnDelete class='btn btn-warning' name=" + stringId + ">Delete</button></td>";
                
                // prikaz samo ako je korisnik prijavljen
                if (token) {
                    row += displayData + displayDelete  + "</tr>";
                } else {
                    row += displayData + "</tr>";
                }
                // dodati red
                tbody.append(row);
            }
            table.append(tbody);

            div.append(table);
           

            // ispis novog sadrzaja
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Greška prilikom preuzimanja Proizvoda!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };
    $("#btnOdustajanje").click(function (e) {
        e.preventDefault();
        $("#regEmail").val('');
        $("#Loz1").val('');
        $("#Loz2").val('');
        $("#start").css("display", "block");
        $("#regDiv").css("display", "none");



    });
    $("#btnPrijava2").click(function (e) {
        e.preventDefault();
        
        $("#prijavaDiv").css("display", "block");
        $("#regDiv").css("display", "none");
        $("#priEmail").val("");
        $("#priLoz").val("");


    });
    $("#btnOdustajanje2").click(function (e) {
        e.preventDefault();
        $("#priEmail").val('');
        $("#priLoz").val('');
        
        $("#start").css("display", "block");
        $("#prijavaDiv").css("display", "none");



    });

    $("#odustajanjeHotel").click(function (e) {
        e.preventDefault();
        refreshTable();



    });
    $("#btnReg").click(function (e) {
        e.preventDefault();
        $("#regDiv").css("display", "block");
        $("#start").css("display", "none");
        $("#inforeg").css("display", "nones");



    });
    $("#btnPrijava").click(function (e) {
        e.preventDefault();
        $("#prijavaDiv").css("display", "block");
        $("#start").css("display", "none");
        $("#infoReg").css("display", "none");
        $("#regDiv").css("display", "none");
        $("#priEmail").val("");
        $("#priLoz").val("");


    });
    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#Loz1").val();
        var loz2 = $("#Loz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'https://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#infoReg").css("display", "block");
            $("#regEmail").val("");
            $("#Loz1").val("");
            $("#Loz2").val("");


        }).fail(function (data) {
            $("#regEmail").val("");
            $("#Loz1").val("");
            $("#Loz2").val("");
            alert("Neuspesna registracija.Pokusajte ponovo.");
        });


    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'https://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijavaDiv").css("display", "none");
            $("#regDiv").css("display", "none");
            $("#prijavljen").css("display", "block");
            refreshTable();
            $("#priEmail").val("");
           $("#priLoz").val("");

        }).fail(function (data) {
            alert("Neuspesna prijava na sistem.Pokusajte ponovo.");
            $("#priEmail").val("");
            $("#priLoz").val("");
        });
    });

    // odjava korisnika sa sistema
    $("#odjava").click(function () {
        token = null;
        headers = {};

        $("#start").css("display", "block");
        
        $("#prijavljen").css("display", "none");
        $("#info").empty();
       
        $("#formProductDiv").css("display", "none");
        refreshTable();

    })

    // forma za rad sa proizvodima
    $("#hotelForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        
        var lanac = $("#lanci").val();
        console.log(lanac);
        var naziv = $("#naziv").val();
        var god = $("#god").val();
        var brojS = $("#brojSoba").val();
        var brojZ = $("#brojZap").val();





        var httpAction = "POST";
        var url = 'https://' + host + hotEndpoint;
        var sendData = {
            "Id":1,
            "Naziv": naziv,
            "LanacId": lanac,
            "Godina": god,
            "BrojSoba": brojS,
            "BrojZaposlenih": brojZ,


        };
    


        // izvrsavanje AJAX poziva
        $.ajax({
        url: url,
        type: httpAction,
        headers: headers,
        data: sendData
    })
            .done(function (data, status) {
                $("#naziv").val('');
                $("#god").val('');
                $("#brojSoba").val('');
                 $("#brojZap").val('');
                refreshTable();
            })
            .fail(function (data, status) {
                $("#naziv").val('');
                $("#god").val('');
                $("#brojSoba").val('');
                $("#brojZap").val('');
                alert("Desila se greska!");
            });

    });

    // forma za rad sa proizvodima
    $("#formPretraga").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var min = $("#min").val();
        var maks = $("#max").val();

        var httpAction = "POST";
        var url = 'https://' + host + "/api/kapacitet";
        var sendData = {
            "Najmanje": min,
            "Najvise": maks
        };

        // izvrsavanje AJAX poziva
        $.ajax({
            url: url,
            type: httpAction,
            headers: headers,
            data: sendData
        })
            .done(function (data, status) {
                $("#min").val('');
                $("#max").val('');
                setHot(data,status);
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
                $("#min").val('');
                $("#max").val('');
            });

    });

    // brisanje proizvoda
    function deleteHot() {
        // izvlacimo {id}
        var deleteID = this.name;

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        // saljemo zahtev 
        $.ajax({
            url: 'https://' + host + hotEndpoint + deleteID.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    };

    function loadLance() {
        var requestUrl = 'https://' + host + "/api/lanci";
        $.getJSON(requestUrl, setLance);
    }
    function setLance(data, status) {
        console.log(data);
        var cont = $("#lanci");
        cont.empty();

        if (status == "success") {
            for (let i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].Id + ">" + data[i].Naziv + "</option>";
                cont.append(option);
            }
        }
        else {
            alert("Greska prilikom preuzimanja lanaca");
        }
    }
    // osvezi prikaz tabele
    function refreshTable() {
        // cistim formu
        $("#lanac").val('');
        $("#naziv").val('');
        $("#god").val('');
        $("#brojSoba").val('');
        $("#brojZap").val('');

        if (token != null) {
            $("#pretraga").css("display", "block");
            loadLance();
            $("#hotel").css("display", "block");

        }
        else {
            $("#pretraga").css("display", "none");
            $("#hotel").css("display", "none");

        }

        // osvezavam
        loadHot();
    };

    $("#ucitavanje").click(function (e) {
        e.preventDefault();

        $("#ucitavanjeDiv").css("display", "none");
        $("#lanciSaTradicijom").css("display", "block");

        if (token) {
            headers.Authorization = "Bearer " + token;
        }
        $.ajax({
            url: 'https://' + host + "/api/tradicija",
            type: "GET",
            headers: headers
        })
            .done(function (data) {
                setTradicija(data, "success");
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    });
    function setTradicija(data, status) {
        var cont = $("#lanciTradicija");
        cont.empty();

        if (status == "success") {
            for (let i = 0; i < data.length; i++) {
                var ol = "<li><b>" + data[i].Naziv + "</b>(osnovan <b>" + data[i].Godina + ".</b> godine)" + "</li>";
                cont.append(ol);
            }
        }
        else {
            alert("Greska prilikom ucitavanja.");
        }
    }

});