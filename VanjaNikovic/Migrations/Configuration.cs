namespace VanjaNikovic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VanjaNikovic.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<VanjaNikovic.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VanjaNikovic.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            Lanac l1 = new Lanac() { Id=1,Naziv="Hilton Worldwide",Godina=1919};
            Lanac l2 = new Lanac() { Id = 2, Naziv = "Marriott International", Godina = 1927 };
            Lanac l3 = new Lanac() { Id = 3, Naziv = "Kempinski", Godina = 1897 };
            context.Lanci.AddOrUpdate(l1, l2, l3);
            context.SaveChanges();

            Hotel h1 = new Hotel() { Id = 1, Naziv = "Sheraton Novi Sad", Godina = 2018, BrojZaposlenih = 70, BrojSoba = 150, LanacId = 2 };
            Hotel h2 = new Hotel() { Id = 2, Naziv = "Hilton Belgrade", Godina = 2017, BrojZaposlenih = 100, BrojSoba = 242, LanacId = 1 };
            Hotel h3 = new Hotel() { Id = 3, Naziv = "Palais Hansen", Godina = 2013, BrojZaposlenih = 80, BrojSoba = 152, LanacId = 3 };
            Hotel h4 = new Hotel() { Id = 4, Naziv = "Budapest Marriott", Godina = 1994, BrojZaposlenih = 130, BrojSoba = 364, LanacId = 2 };
            Hotel h5 = new Hotel() { Id = 5, Naziv = "Hilton Berlin", Godina = 1991, BrojZaposlenih = 200, BrojSoba = 601, LanacId = 1 };
            context.Hoteli.AddOrUpdate(h1, h2, h3, h4, h5);
            context.SaveChanges();

        }
    }
}
