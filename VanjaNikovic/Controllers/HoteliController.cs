﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VanjaNikovic.Interfaces;
using VanjaNikovic.Models;

namespace VanjaNikovic.Controllers
{
    public class HoteliController : ApiController
    {
        IHotelRepo _repository;
        public HoteliController(IHotelRepo repo)
        {
            _repository = repo;
        }
        [Route("api/hoteli")]
        public IEnumerable<Hotel> GetHoteli()
        {
            return _repository.GetAll();
        }

        [Route("api/hoteli/")]
        public IEnumerable<Hotel> GetZaposleni(int zaposleni)
        {
            return _repository.GetMinimum(zaposleni);
        }

        [Route("api/hoteli/{id}")]

        public IHttpActionResult GetHotel(int id)
        {
            var hotel = _repository.GetById(id);
            if (hotel == null)
            {
                return NotFound();
            }
            return Ok(hotel);
        }

        [Route("api/hoteli/")]
       
        public IHttpActionResult PostHotel(Hotel hotel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(hotel);
            return Ok(hotel) ;
        }
        [Route("api/hoteli")]
        
        public IHttpActionResult Put(int id, Hotel hotel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hotel.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(hotel);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(hotel);
        }
        [Route("api/hoteli/{id}")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            var hotel = _repository.GetById(id);
            if (hotel == null)
            {
                return NotFound();
            }

            _repository.Delete(hotel);
            return Ok();
        }
        [Authorize]
        [Route("api/kapacitet")]
        public IEnumerable<Hotel> PostPretraga(Pretraga pretraga)
        {
            return _repository.GetPretraga(pretraga);
        }
    }
}
