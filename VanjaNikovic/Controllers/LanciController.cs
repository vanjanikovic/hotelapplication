﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VanjaNikovic.Interfaces;
using VanjaNikovic.Models;

namespace VanjaNikovic.Controllers
{
    public class LanciController : ApiController
    {
        ILanacRepo _repository;
        public LanciController(ILanacRepo repo)
        {
            _repository = repo;
        }
        [Route("api/lanci")]
        public IEnumerable<Lanac> GetLanci()
        {
            return _repository.GetAll();
        }
        [Route("api/lanci/{id}")]

        public IHttpActionResult Getlanac(int id)
        {
            var lanac = _repository.GetById(id);
            if (lanac == null)
            {
                return NotFound();
            }
            return Ok(lanac);
        }
        [Route("api/tradicija")]
        [Authorize]
        public IEnumerable<Lanac> GetTradicija()
        {
            return _repository.GetTradicija();
        }
        [Route("api/zaposleni")]
        public IEnumerable<LanciZaposleni> GetZaposleni()
        {
            return _repository.GetZaposleni();
        }
        [Route("api/sobe")]
        public IEnumerable<LanciPost> PostSobe(int granica)
        {
            return _repository.PostUkupanBrojSoba(granica);
        }
    }
}
