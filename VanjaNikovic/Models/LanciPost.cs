﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VanjaNikovic.Models
{
    public class LanciPost
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int Godina { get; set; }
        public int BrojSoba { get; set; }
    }
}