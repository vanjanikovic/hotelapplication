﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using VanjaNikovic.Controllers;
using VanjaNikovic.Interfaces;
using VanjaNikovic.Models;

namespace VanjaNikovic.Tests.Controllers
{
    [TestClass]
    public class HoteliControllerTests
    {
        [TestMethod]
        public void GetReturnsHotWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepo>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Hotel { Id = 42 });

            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetHotel(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Hotel>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepo>();
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Hotel { Id = 9 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Hotel> hoteli = new List<Hotel>();
            hoteli.Add(new Hotel { Id = 1 });
            hoteli.Add(new Hotel { Id = 2 });

            var mockRepository = new Mock<IHotelRepo>();
            mockRepository.Setup(x => x.GetAll()).Returns(hoteli.AsEnumerable());
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IEnumerable<Hotel> result = controller.GetHoteli();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(hoteli.Count, result.ToList().Count);
            Assert.AreEqual(hoteli.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(hoteli.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void PostReturnsMultipleObjects()
        {
            // Arrange
            List<Hotel> hoteli = new List<Hotel>();
            hoteli.Add(new Hotel { Id = 1 });
            hoteli.Add(new Hotel { Id = 2 });
            Pretraga p = new Pretraga();
            var mockRepository = new Mock<IHotelRepo>();
            mockRepository.Setup(x => x.GetPretraga(p)).Returns(hoteli.AsEnumerable());
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IEnumerable<Hotel> result = controller.PostPretraga(p);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(hoteli.Count, result.ToList().Count);
            Assert.AreEqual(hoteli.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(hoteli.ElementAt(1), result.ElementAt(1));

        }
    }
}
